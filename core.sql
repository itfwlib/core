-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2018 at 01:06 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baseproyek`
--

-- --------------------------------------------------------

--
-- Dumping data for table `basecrud`
--
CREATE TABLE `coa` (
  `no_akun` char(5) NOT NULL,
  `nama_akun` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_absensi_rfid`
--

CREATE TABLE `jurnal` (
  `id_jurnal` int(11) NOT NULL,
  `no_akun` char(5) NOT NULL,
  `posisi_dr_cr` varchar(6) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_trans` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
CREATE TABLE `transaksi` (
  `id_trans` varchar(30) NOT NULL,
  `tgl_trans` date DEFAULT NULL,
  `jml_trans` int(11) DEFAULT NULL,
  `created_by` varchar(31) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

ALTER TABLE `coa`
  ADD PRIMARY KEY (`no_akun`);

--
-- Indexes for table `detail_absensi_rfid`
--

ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id_jurnal`,`no_akun`,`id_trans`),
  ADD KEY `fk_id_trans` (`id_trans`),
  ADD KEY `fk_no_akun` (`no_akun`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `user_account`
--

--
-- AUTO_INCREMENT for dumped tables
--

ALTER TABLE `jurnal`
  MODIFY `id_jurnal` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

ALTER TABLE `jurnal`
  ADD CONSTRAINT `fk_id_trans` FOREIGN KEY (`id_trans`) REFERENCES `transaksi` (`id_trans`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_no_akun` FOREIGN KEY (`no_akun`) REFERENCES `coa` (`no_akun`) ON DELETE CASCADE ON UPDATE CASCADE;