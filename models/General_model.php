<?php
	class General_model extends CI_MODEL{

		public function get_new_id_item($table){
			$prefix = get_prefix($table);
			$length = strlen($prefix);
			$nextNumber = $this->numberNewItem($table,$length);
			return $prefix.'-'.$nextNumber;
		}
		public function getKeyColoumn($table){
			$show_keys = $this->db->query("SHOW KEYS FROM ".$table." WHERE Key_name = 'PRIMARY'")->row();
			$primary_key = $show_keys->Column_name;
			return $primary_key;
		}
		public function numberNewItem($table,$length){
			$primary_key = $this->getKeyColoumn($table);
			$currentMax = $this->db->query("select max(substring(".$primary_key.",5,".$length.")*1) pk from ".$table." ")->row()->pk;
			return $numberNewItem = $currentMax+1;
		}
		public function getTransID($table,$insertTable = array('status' => false,'data' => array())){
			$prefix = get_prefix($table);
			$primary = $this->getKeyColoumn($table);
			// check table status
				$this->db->where('status','0');
				$this->db->select($primary.' as key');
				$q = $this->db->get($table);

			if (count($q->result()) > 0) {
				return $q->row()->key;
			}else{
				$newID = $this->get_new_id_item($table);

				$arr = array(
					'id_trans' => $newID,
					'tgl_trans' => date('Y-m-d'),
					'created_by' => $this->session->userdata('username'));
				$this->db->insert('transaksi',$arr);
				if($insertTable['status']){
					$arr = array();
					$arr = @$insertTable['data'];
					$arr[$primary] = $newID;
					$arr['status'] = '0';
					$this->db->insert($table,$arr);
				}
				return $newID;
			}
		}
		public function saveTransaksi($params){
			$data = array(
				'id_trans' => $params['id_trans'],
				'tgl_trans' => $params['tgl_trans'],
				'jml_trans' => $params['jml_trans'],
				'created_by' => $this->session->userdata('username')
			);
			$this->db->insert('transaksi',$data);
			$result = array(
				'status' => true,
				'data' => $params['id_trans'],
				'info' => 'sukses');
			return $result;
		}
		public function getResult($status = true,$info = 'Sukses'){
			return array(
				'code' => '0',
				'status' => $status,
				'info' => $info);
		}
		public function result($code = 200,$info = 'Success',$data = null){
			$result = new stdClass;
			$result->code = $code;
			$result->info = $info;
			$result->data = $data;
			return $result;
		}
	}